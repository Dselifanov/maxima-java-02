package org.example;

import org.junit.Assert;
import org.junit.Test;

public class TransporFactoryTest {
    City moscow = new City("Moscow", 1200, true, true);
    City izhevsk = new City("Izhevsk", 10000, true, true);
    City newYork = new City("NewYork", 1800, true, true);
    City kaluga = new City("Kaluga", 600, true, true);

    @Test
    public void shouldBeTransportName() {
        Assert.assertEquals("Kamaz", TransportFactory.getTransport(moscow, 2199, 12).getName());
        Assert.assertEquals("Летучий голандец", TransportFactory.getTransport(izhevsk, 2199, 350).getName());
        Assert.assertEquals("Airbus", TransportFactory.getTransport(newYork, 2199, 12).getName());
        Assert.assertEquals("Летучий голандец", TransportFactory.getTransport(kaluga, 2199, 120).getName());
    }

    @Test
    public void shouldBeRoundingEquals500(){
        Assert.assertEquals(500,TransportFactory.getTransport(kaluga, 500, 120).getCapacity());
        Assert.assertEquals(10000,TransportFactory.getTransport(newYork, 9650, 120).getCapacity());
        Assert.assertEquals(2500,TransportFactory.getTransport(moscow, 2500, 120).getCapacity());
        Assert.assertEquals(500,TransportFactory.getTransport(izhevsk, 499, 120).getCapacity());
    }

    @Test
    public void shouldBeRoundingSpeed10(){
        Assert.assertEquals(10,TransportFactory.getTransport(kaluga, 500, 119).getSpeed());
        Assert.assertEquals(20,TransportFactory.getTransport(newYork, 9650, 121).getSpeed());
        Assert.assertEquals(140,TransportFactory.getTransport(moscow, 2500, 9).getSpeed());
        Assert.assertEquals(90,TransportFactory.getTransport(izhevsk, 499, 120).getSpeed());
    }
}
