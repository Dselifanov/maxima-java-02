package org.example;

public class Ship extends Transport {

        private String name;

    public Ship(String name, int capacity, int speed, float costOfKm) {
        super(name, capacity, speed, costOfKm);
    }

    @Override
    public int getPrice(City city) {
        int costOfTrans;
        if (!city.isOnWater())
            return 0;

        costOfTrans = (int) (this.getCostOfKm() * city.getDistanceKm());

        return costOfTrans;
    }

}
