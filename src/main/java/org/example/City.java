package org.example;

public class City {

        private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

        private int distanceKm;



    public int getDistanceKm() {

        return distanceKm;
    }

    public void setDistanceKm(int distanceKm) {

        this.distanceKm = distanceKm;
    }
        private boolean hasAirport;

    public boolean hasAirport() {
        return hasAirport;
    }

    public void setHasAirport(boolean hasAirport) {
        this.hasAirport = hasAirport;
    }



    private boolean isOnWater;

    public boolean isOnWater() {
        return isOnWater;
    }

    public void setOnWater(boolean onWater) {
        isOnWater = onWater;
    }

    public City(String name, int distanceKm) {
        this.name = name;
        this.distanceKm = distanceKm;

    }
    public City(String name, int distanceKm,  boolean isOnWater, boolean hasAirport) {
        this.name = name;
        this.distanceKm = distanceKm;
        this.isOnWater = isOnWater;
        this.hasAirport = hasAirport;
    }

}
