package org.example;

public class TransportFactory {

    static Transport getTransport(City city, int weight, int hours){

        final String PLANE_NAME = "Airbus";
        final int PLANE_COST = 1000;
        final String TRUCK_NAME = "Kamaz";
        final int TRUCK_COST = 60;
        final String SHIP_NAME = "Летучий голандец";
        final int SHIP_COST = 2000;


        int speedOrder = city.getDistanceKm()/hours;
       // System.out.println(speedOrder);

        int capacity = (weight%500 != 0) ? (weight+(500-weight%500)) : weight;
       // System.out.println(capacity);

        int speed = (speedOrder%10 != 0) ? (speedOrder+(10-speedOrder%10)):speedOrder;
        //System.out.println(speed);

        if (speed< 40 & city.isOnWater()){
            return new Ship(SHIP_NAME,capacity, speed,SHIP_COST);
        }
        if (speed>120 & city.hasAirport()){
            return new Plane(PLANE_NAME, capacity, speed, PLANE_COST);
        }

        return new Truck(TRUCK_NAME, capacity, speed, TRUCK_COST);
    }

}
